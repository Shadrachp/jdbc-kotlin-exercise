package com.shadrachp.exer007

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Exer007

fun main(args: Array<String>) {
	runApplication<Exer007>(*args)
}